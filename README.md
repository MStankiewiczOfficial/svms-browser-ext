# SVMS New Tab & Search Provider - Chromium and Firefox Extension

This extension defaults SVMetaSearch as a search engine. It also replaces the new tab page with SVMetaSearch home page (only in Chromium version).

## Installation

### Extension stores

If you use a Chromium-based browser, like [Google Chrome](https://www.google.com/chrome/), [Microsoft Edge](https://www.microsoft.com/edge), [Opera](https://www.opera.com/) or [Ungoogled Chromium](https://github.com/ungoogled-software/ungoogled-chromium) (with [Chromium Web Store extension](https://github.com/NeverDecaf/chromium-web-store)), you can download the extension from [Chrome Web Store](https://chrome.google.com/webstore/detail/ogmmlpalikhngdicenifnggfjbodjljk).

If you use Mozilla Firefox, you can download the extension from [Firefox Add-ons](https://addons.mozilla.org/firefox/addon/svms-search-provider/).

### Manual installation

Using extension stores are recommended. However, if you want to install the extension manually, you can do so by following the steps below:

1. Download the extension from the [releases page](https://codeberg.org/SVWareHouse/svms-browser-ext/releases).
2. Extract the downloaded archive.
3. If you use Google Chrome, Microsoft Edge, or any other popular Chromium-based browser, open the browser and go to `chrome://extensions`. If you use Mozilla Firefox, open the browser and go to `about:debugging#/runtime/this-firefox`.

- In Google Chrome, Microsoft Edge, or any other popular Chromium-based browser, enable the developer mode by clicking the toggle switch in the top-right corner. In Mozilla Firefox, click the `Enable add-on debugging` button.

4. In Google Chrome, Microsoft Edge, or any other popular Chromium-based browser, click the `Load unpacked` button and select the extracted folder. In Mozilla Firefox, click the `Load Temporary Add-on` button and select the `manifest.json` file in the extracted folder.

## For Quklook users

Project *Quklook* by Michał Stankiewicz **is no longer maintained.** An author of this extension is the same person as the author of Quklook (Michał Stankiewicz) and his cooperating with the author of SVMetaSearch (Oliwier Jaszczyszyn). This extension is a replacement for Quklook New Tab & Search Provider for SVMS.
If you use Quklook New Tab & Search Provider from Chrome Web Store, extension will be replaced soon after the update. If you use Quklook New Tab & Search Provider from Firefox Add-ons, you have to uninstall it manually and install new extension.

## Contact

We have a room [on Matrix](https://matrix.to/#/%23svmetasearch:noevil.pl) - have a say in SVMetaSearch development!
If you don't have a Matrix account, you can mail the extension's author at [kontakt at mstankiewi.cz](mailto:kontakt@mstankiewi.cz).
